package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"golang.org/x/net/context"

	"github.com/gorilla/mux"
	"github.com/rs/cors"

	"cloud.google.com/go/pubsub"
)

type Lead struct {
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
	Email     string `json:"email,omitempty"`
}

func (lead Lead) mapit() (leadMap map[string]string) {
	leadMap = map[string]string{
		"firstName": lead.FirstName,
		"lastName":  lead.LastName,
		"email":     lead.Email,
	}
	return
}

func GetStatus(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("OK"))
}

func CreateLead(w http.ResponseWriter, req *http.Request) {
	var lead Lead
	_ = json.NewDecoder(req.Body).Decode(&lead)
	fmt.Println(lead)

	ctx := context.Background()
	// [START auth]
	proj := os.Getenv("GOOGLE_CLOUD_PROJECT")
	if proj == "" {
		fmt.Fprintf(os.Stderr, "GOOGLE_CLOUD_PROJECT environment variable must be set.\n")
		os.Exit(1)
	}
	client, err := pubsub.NewClient(ctx, proj)
	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}
	// [END auth]

	const topic = "kewit"
	msg := "Test message"

	t := client.Topic(topic)
	result := t.Publish(ctx, &pubsub.Message{
		Data:       []byte(msg),
		Attributes: lead.mapit(),
	})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	id, _ := result.Get(ctx)
	fmt.Printf("Published a message; msg ID: %v\n", id)

	// w.Write([]byte("Sent"))
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/lead", CreateLead).Methods("POST")
	router.HandleFunc("/", GetStatus).Methods("GET")

	handler := cors.Default().Handler(router)
	log.Fatal(http.ListenAndServe(":80", handler))
}
