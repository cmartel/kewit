/**
 * Cloud Function.
 *
 * @param {object} event The Cloud Functions event.
 * @param {function} callback The callback function.
 */
exports.kewitPusher = function kewitPusher (event, callback) {

  var Pusher = require('pusher');

  var pusher = new Pusher({
    appId: '438682',
    key: '484dd61e26fb83a28b66',
    secret: 'fc722060223532afbb41',
    cluster: 'eu',
    encrypted: true
  });

  pusher.trigger('my-channel', 'my-event', {
    "message": event
  });

  callback();
};

