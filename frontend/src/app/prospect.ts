export class Prospect {
  constructor(
    public firstName: string,
    public lastName: string,
    public email: string
  ) {  }
}
