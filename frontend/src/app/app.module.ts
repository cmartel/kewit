import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProspectFormComponent } from './prospect-form/prospect-form.component';
import { PusherLogComponent } from './pusher-log/pusher-log.component';
import { PusherService } from './pusher.service';



@NgModule({
  declarations: [
    AppComponent,
    ProspectFormComponent,
    PusherLogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PusherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
