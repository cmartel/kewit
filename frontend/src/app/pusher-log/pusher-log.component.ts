import { Component, OnInit } from '@angular/core';

import { PusherService } from '../pusher.service';

interface Message {
  lastName: string;
  firstName: string;
  email: string;
}
export class MessagesComponent {
  messages: Array<Message>;
  constructor() {
    this.messages = [];
  }
}


@Component({
	selector: 'app-pusher-log',
	templateUrl: './pusher-log.component.html',
	styleUrls: ['./pusher-log.component.css']
})

export class PusherLogComponent implements OnInit {

	messages: Array<Message>;
	constructor(private pusherService: PusherService) {
		this.messages = [];
	};

  	ngOnInit() {
   		this.pusherService.messagesChannel.bind('my-event', (message) => {
      		this.messages.push(message.message);
    	});
  	}

  	// TODO: Remove this when we're done


	// msg = new Message();
	// msg.evenId = '123456'
	// Pusher.logToConsole = true;
	// pusher = new Pusher('484dd61e26fb83a28b66', {
	// 	cluster: 'eu',
	// 	encrypted: true
	// });
	// channel = this.pusher.subscribe('my-channel');
	// channel.bind('my-event', function(data) {
	// 	console.dir(data);
	// });

}