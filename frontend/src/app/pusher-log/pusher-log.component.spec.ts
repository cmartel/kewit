import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PusherLogComponent } from './pusher-log.component';

describe('PusherLogComponent', () => {
  let component: PusherLogComponent;
  let fixture: ComponentFixture<PusherLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PusherLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PusherLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
