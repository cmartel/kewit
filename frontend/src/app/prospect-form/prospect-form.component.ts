import { Component } from '@angular/core';
import { Prospect } from '../prospect';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-prospect-form',
  templateUrl: './prospect-form.component.html',
  styleUrls: ['./prospect-form.component.css']
})
export class ProspectFormComponent {

  //prospect = new Prospect("Chris", "Martel", "christoph.martel@gmail.com");
  prospect = new Prospect("", "", "");

  constructor(private http: HttpClient) {};

  submitted = false;

  onSubmit() {
    const req = this.http.post('http://app.codeways.site/lead', this.prospect);
	  req.subscribe();
    this.submitted = true;
  }

  get diagnostic() { return JSON.stringify(this.prospect); }
}
