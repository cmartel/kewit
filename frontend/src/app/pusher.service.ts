import { environment } from '../environments/environment';

declare const Pusher: any;

export class PusherService {
  pusher: any;
  messagesChannel: any;

  constructor() {
  	Pusher.logToConsole = true;
    this.pusher = new Pusher(environment.pusher.key, {
		cluster: 'eu',
		encrypted: true
	});
	this.messagesChannel = this.pusher.subscribe('my-channel');
  }
}