
.PHONY: docker backend publish run

init:
	export GOOGLE_CLOUD_PROJECT=kube-playground-185721
	export GOOGLE_APPLICATION_CREDENTIALS=backend/kube-playground-4990a1cfdee2.json

run: init
	cd frontend && ng serve&
	cd backend && go run main.go

publish:
	cd frontend && gsutil rsync -R dist gs://www.codeways.site
	cd frontend && gsutil acl ch -r -u AllUsers:R gs://www.codeways.site/*
	cd frontend && gsutil web set -m index.html gs://www.codeways.site

docker:
	docker build -f docker/Dockerfile-gcloud -t registry.gitlab.com/cmartel/kewit/gcloud:0.1 .
	docker push registry.gitlab.com/cmartel/kewit/gcloud
	docker build -f docker/Dockerfile-ng -t registry.gitlab.com/cmartel/kewit/ng:0.1 .
	docker push registry.gitlab.com/cmartel/kewit/ng

backend:
	cd backend && go build -o kewit
	docker build -f docker/Dockerfile-app -t registry.gitlab.com/cmartel/kewit/app:latest .

kube:
	APISERVER=$(kubectl config view | grep server | cut -f 2- -d ":" | tr -d " ")
	TOKEN=$(kubectl describe secret $(kubectl get secrets | grep default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t')

gcf:
	cd gcf_pusher && gcloud beta functions deploy kewitPusher --stage-bucket kewit_gcf_pusher --trigger-topic kewit --project kube-playground-185721